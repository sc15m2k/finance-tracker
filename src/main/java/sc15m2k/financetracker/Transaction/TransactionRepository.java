package sc15m2k.financetracker.Transaction;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {
    Transaction findByPayee(String payee);

    Transaction findById(Integer id);

    List<Transaction> findAllByAccount_NameAndDateBetween(String accName, Date start, Date end);

    List<Transaction> findAllByAccount_Name(String accName);

    List<Transaction> findAllByDateBetween(Date start, Date end);
}
