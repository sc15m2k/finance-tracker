package sc15m2k.financetracker.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import sc15m2k.financetracker.Account.Account;
import sc15m2k.financetracker.Account.AccountRepository;
import sc15m2k.financetracker.ErrorMessage;
import sc15m2k.financetracker.User.User;
import sc15m2k.financetracker.User.UserRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/transaction", produces="application/json", consumes="application/json")
public class TransactionController {

    //produces="application/json", consumes="application/json"
    //@PostMapping(path = "transaction/") -> Add a transaction
    //@GetMapping(path = "transaction/") -> Get all transactions
    //@GetMapping(path = "transaction/filter") -> Filter transactions by given request parameter
    //@GetMapping(path = "transaction/{id}") -> Get a single transaction
    //@DeleteMapping(path = "transaction/{id}") -> Delete transaction

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;

    @PostMapping(path = "/")
    public ResponseEntity<?> addNewTransaction(@RequestBody Transaction trans) {

        if (trans == null) {
            return new ResponseEntity<>(new ErrorMessage("Request body is empty."), HttpStatus.NOT_FOUND);
        }

        // change the balance of the account after each transaction
        Account currentAcc = accountRepository.findByAccountId(trans.getAccount().getId());
        //withdrawal
        if (trans.getType().equals("withdrawal")) {
            currentAcc.setBalance(currentAcc.getBalance() - trans.getAmount());
        }
        //deposit
        else {
            currentAcc.setBalance(currentAcc.getBalance() + trans.getAmount());
        }
        transactionRepository.save(trans);
        accountRepository.save(currentAcc);

        return new ResponseEntity<>(trans, HttpStatus.CREATED);
    }

    @GetMapping(path = "/")
    public ResponseEntity<?> getAllTransactions() {

        Iterable<Transaction> source = transactionRepository.findAll();
        ArrayList<Transaction> target = new ArrayList<>();
        for (Transaction trans : source) {
            target.add(trans);
        }
        return new ResponseEntity<List<Transaction>>(target, HttpStatus.OK);
    }

    @GetMapping(path = "/filter")
    public ResponseEntity<?> getAllTransactions(@RequestParam(value = "type") String type,
                                                @RequestParam(value = "start", required = false) String start,
                                                @RequestParam(value = "end", required = false) String end) {

        Date startDate = null;
        Date endDate = null;
        System.out.println("start:" + start);
        System.out.println("end:" + end);
        // set default values if not present from the HTTP request
        if (StringUtils.isEmpty(start) || StringUtils.isEmpty(start)) {
            startDate = new Date(0);
            endDate = new Date();
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            try {
                startDate = dateFormat.parse(start);
                endDate = dateFormat.parse(end);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        // get a reference to the accounts of the currently signed in user
        User currentUser = userRepository.findBySignedInIsTrue();
        Set<Account> accounts = currentUser.getAccounts();

        // get all transactions for the currently signed in user
        ArrayList<Transaction> transactions = new ArrayList<>();
        for (Account acc : accounts) {
            transactions.addAll(transactionRepository.findAllByAccount_NameAndDateBetween(acc.getName(), startDate, endDate));
        }
        System.out.println("transactions by account name and date:" + transactions);

        ArrayList<Transaction> target = new ArrayList<>();
        for (Transaction trans : transactions) {
            // check the type of transaction
            if (trans.getType().equals(type)) {
                target.add(trans);
            }
        }

        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getTransaction(@PathVariable int id) {

        Transaction trans = transactionRepository.findById(id);
        if (trans == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such transaction."), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(trans, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable int id) {

        Transaction trans = transactionRepository.findById(id);
        if (trans == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such transaction."), HttpStatus.NOT_FOUND);
        }
        transactionRepository.delete(trans);
        return new ResponseEntity<>(trans, HttpStatus.OK);
    }
}
