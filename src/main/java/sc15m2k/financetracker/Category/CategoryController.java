package sc15m2k.financetracker.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sc15m2k.financetracker.ErrorMessage;
import sc15m2k.financetracker.User.User;
import sc15m2k.financetracker.User.UserRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "/category", produces = "application/json", consumes = "application/json")
public class CategoryController {

    //produces="application/json", consumes="application/json"
    //@PostMapping(path = "category/") -> Add a category
    //@GetMapping(path = "category/") -> Get all categories
    //@GetMapping(path = "category/{id}") -> Get a single category
    //@GetMapping(path = "category/filter") -> Get categories based on type
    //@DeleteMapping(path = "category/{id}") -> Delete a category

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private UserRepository userRepository;

    @PostMapping(path = "/")
    public ResponseEntity<?> addNewCategory(@RequestBody Category cat) {

        //check if such a category already exist
        Category existingCat = categoryRepository.findByName(cat.getName());
        if (existingCat != null) {
            return new ResponseEntity<>(new ErrorMessage("Such a category already exist."), HttpStatus.CONFLICT);
        }

        //get currently signed in user and set it to the category
        User currentUser = userRepository.findBySignedInIsTrue();
        cat.setUser(currentUser);

        categoryRepository.save(cat);
        return new ResponseEntity<>(cat, HttpStatus.CREATED);
    }

    @GetMapping(path = "/")
    public ResponseEntity<?> getAllCategories() {

        Iterable<Category> source = categoryRepository.findAll();
        ArrayList<Category> target = new ArrayList<>();
        for (Category cat : source) {
            target.add(cat);
        }
        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getCategory(@PathVariable Integer id) {

        Category cat = categoryRepository.findByCategoryId(id);
        if (cat == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such category."), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cat, HttpStatus.OK);
    }

    @GetMapping(path = "/filter")
    public ResponseEntity<?> getCategoriesByType(@RequestParam(value = "type") String type) {

        // get a reference to the accounts of the currently signed in user
        User currentUser = userRepository.findBySignedInIsTrue();
        Set<Category> categories = currentUser.getCategories();

        //check the type of category
        Set<Category> target = new HashSet<>();
        for (Category cat : categories) {
            if (cat.getType().equals(type))
                target.add(cat);
        }
        return new ResponseEntity<>(target, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{name}")
    public ResponseEntity<?> deleteAccount(@PathVariable String name) {

        if (name == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such category."), HttpStatus.NO_CONTENT);
        }
        Category cat = categoryRepository.findByName(name);
        System.out.println("cat:" + cat);
        System.out.println("name:" + name);
        if (cat == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such category."), HttpStatus.NOT_FOUND);
        }
        categoryRepository.delete(cat);
        return new ResponseEntity<>(cat, HttpStatus.OK);
    }
}
