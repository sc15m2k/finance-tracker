package sc15m2k.financetracker.Category;

import org.springframework.data.repository.CrudRepository;
import sc15m2k.financetracker.Transaction.Transaction;

import java.util.List;
import java.util.Set;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

    Category findByCategoryId(Integer id);
    Category findByName(String name);
    Set<Category> findAllByType(String type);

}
