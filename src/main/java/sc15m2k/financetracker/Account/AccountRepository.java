package sc15m2k.financetracker.Account;

import org.springframework.data.repository.CrudRepository;
import sc15m2k.financetracker.User.User;

import java.util.Set;

public interface AccountRepository extends CrudRepository<Account, Integer> {

    Account findByNameAndUser(String name, User user);

    Account findByName(String name);

    Account findByAccountId(Integer id);

    Set<Account> findAllByUser(User user);
}
