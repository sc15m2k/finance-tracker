package sc15m2k.financetracker.Account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sc15m2k.financetracker.ErrorMessage;
import sc15m2k.financetracker.User.User;
import sc15m2k.financetracker.User.UserRepository;

import java.util.Set;

@RestController
@RequestMapping(path = "/account", produces = "application/json", consumes = "application/json")
public class AccountController {

    //produces="application/json", consumes="application/json"
    //@PostMapping(path = "account/") -> Add an account
    //@GetMapping(path = "account/") -> Get all account
    //@GetMapping(path = "account/{id}") -> Get a single account
    //@PutMapping(path = "account/{id}") -> Update an account
    //@DeleteMapping(path = "account/{id}") -> Delete an account

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserRepository userRepository;

    @PostMapping(path = "/")
    public ResponseEntity<?> addNewAccount(@RequestBody Account acc) {

        //Set the currently signed in user as User entity of the newly created account
        User currentUser = userRepository.findBySignedInIsTrue();
        acc.setUser(currentUser);
        accountRepository.save(acc);

        return new ResponseEntity<>(acc, HttpStatus.CREATED);
    }

    @GetMapping(path = "/")
    public ResponseEntity<?> getAllAccounts() {

        User currentUser = userRepository.findBySignedInIsTrue();
        Set<Account> accounts = accountRepository.findAllByUser(currentUser);
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getAccount(@PathVariable int id) {

        Account acc = accountRepository.findByAccountId(id);
        if (acc == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such account."), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(acc, HttpStatus.OK);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<?> editAccount(@PathVariable int id, @RequestBody Account acc) {

        Account currentAcc = accountRepository.findByAccountId(id);
        if (currentAcc == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such account."), HttpStatus.NOT_FOUND);
        }

        currentAcc.setName(acc.getName());
        currentAcc.setBalance(acc.getBalance());
        accountRepository.save(currentAcc);

        return new ResponseEntity<>(currentAcc, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable int id) {

        Account currentAcc = accountRepository.findByAccountId(id);
        if (currentAcc == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such account."), HttpStatus.NOT_FOUND);
        }
        accountRepository.delete(currentAcc);
        return new ResponseEntity<>(currentAcc, HttpStatus.OK);
    }

    //Used for testing purposes ONLY
    @PostMapping(path = "/test")
    public ResponseEntity<?> testAccount() {

        User currentUser = userRepository.findBySignedInIsTrue();
        Account acc = new Account("test", 100, currentUser);
        accountRepository.save(acc);

        return new ResponseEntity<>(acc, HttpStatus.CREATED);
    }

    @GetMapping(path = "/test/{name}")
    public ResponseEntity<?> getAccountByName(@PathVariable String name) {

        Account acc = accountRepository.findByName(name);
        if (acc == null) {
            return new ResponseEntity<>(new ErrorMessage("There is no such account."), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(acc, HttpStatus.OK);
    }
}
