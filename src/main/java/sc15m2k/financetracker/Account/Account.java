package sc15m2k.financetracker.Account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import sc15m2k.financetracker.Transaction.Transaction;
import sc15m2k.financetracker.User.User;

import javax.persistence.*;
import java.util.Set;

@Entity // This tells Hibernate to make a table out of this class
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer accountId;
    private String name;
    private Integer balance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", nullable = false)
    @JsonIgnore
    private User user;

    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, mappedBy = "account")
    @JsonIgnore
    private Set<Transaction> transactions;

    public Account() {
    }

    public Account(String name, Integer balance, User user) {
        this.name = name;
        this.balance = balance;
        this.user = user;
    }

    public Integer getId() {
        return accountId;
    }

    public void setId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}
