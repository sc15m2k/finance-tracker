package sc15m2k.financetracker.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sc15m2k.financetracker.ErrorMessage;

@RestController
@RequestMapping(path = "/user", produces="application/json", consumes="application/json")
public class UserController {

    //produces="application/json", consumes="application/json"
    //@PostMapping(path = "user/") -> Add a user
    //@GetMapping(path = "user/filter") -> Get signed in user
    //@GetMapping(path = "user/{email:.+}") -> Get a single users by email
    //@PutMapping(path = "user/{id}") -> Update a user (change the value of signedIn)

    @Autowired
    private UserRepository userRepository;

    @PostMapping(path = "/")
    public ResponseEntity<?> addNewUser(@RequestBody User user) {

        User existingUser = userRepository.findByEmail(user.getEmail());
        if (existingUser != null) {
            return new ResponseEntity<>(new ErrorMessage("User with such email already exist."), HttpStatus.CONFLICT);
        }
        userRepository.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping(path = "/filter")
    public ResponseEntity<?> getAllUsers() {

        User signedInUser = userRepository.findBySignedInIsTrue();
        return new ResponseEntity<>(signedInUser, HttpStatus.OK);
    }

    @GetMapping(path = "/{email:.+}")
    public ResponseEntity<?> getUser(@PathVariable String email) {

        User user = userRepository.findByEmail(email);
        System.out.println(email);
        if (user == null) {
            return new ResponseEntity<>(new ErrorMessage("Unable to find such user."), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(path = "/")
    public ResponseEntity<?> updateUser(@RequestBody User user) {

        if (user == null) {
            return new ResponseEntity<>(new ErrorMessage("Request body is empty."), HttpStatus.NOT_FOUND);
        }

        //get the signed in user if we want to sign out
        User currentUser = userRepository.findBySignedInIsTrue();
        if (currentUser == null) {
            //find the user that wants to sign in
            currentUser = userRepository.findByEmail(user.getEmail());
        }
        //set the signedIn value accordingly
        currentUser.setSignedIn(user.getSignedIn());
        userRepository.save(currentUser);
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }
}
