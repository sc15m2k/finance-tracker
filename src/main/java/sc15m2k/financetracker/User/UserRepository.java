package sc15m2k.financetracker.User;

import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String email);
    User findBySignedInIsTrue();
}
