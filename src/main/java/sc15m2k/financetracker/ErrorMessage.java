package sc15m2k.financetracker;

public class ErrorMessage {
    private String errMessage;

    public ErrorMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }
}
