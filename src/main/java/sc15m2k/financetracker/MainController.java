package sc15m2k.financetracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import sc15m2k.financetracker.Account.Account;
import sc15m2k.financetracker.Account.AccountRepository;
import sc15m2k.financetracker.Transaction.Transaction;
import sc15m2k.financetracker.Transaction.TransactionRepository;
import sc15m2k.financetracker.User.User;

@Controller
public class MainController {

    @GetMapping("/")
    String index() {
        // render the html file
        return "dashboard";
    }

    @GetMapping("/dashboard")
    String dashboard() {
        // render the html file
        return "dashboard";
    }

    @GetMapping("/account")
    String account() {
        // render the html file
        return "account";
    }

    @GetMapping("/account-form")
    String accountForm() {
        // render the html file
        return "account_form";
    }

    @GetMapping("/transaction")
    String transaction() {
        // render the html file
        return "transaction";
    }

    @GetMapping("/transaction-form")
    String transactionForm() {
        // render the html file
        return "transaction_form";
    }

    @GetMapping("/category")
    String category() {
        // render the html file
        return "category";
    }

    @GetMapping("/category-form")
    String categoryForm() {
        // render the html file
        return "category_form";
    }

    @GetMapping("/signin")
    public String signinForm() {
        // render the html file
        return "signin";
    }

    @GetMapping("/register")
    public String registerForm() {
        // render the html file
        return "register";
    }
}
